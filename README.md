# Desenvolvedor Android (Java)

## Introdução

Olá, se você está lendo reste documento é porque possivelmente foi selecionado para dar uma palha do que é capaz de fazer. Seja bem vindo, mão na massa!

## O Teste: Pokédex

Crie um aplicativo usando a IDE do Android Studio e automatizador de builds Gradle que simule uma Pokédex ([O que é uma Pokédex?](https://www.google.com.br/search?q=o+que+%C3%A9+uma+pokedex&ie=utf-8&oe=utf-8&client=firefox-b-ab&gfe_rd=cr&ei=zdSgV9OGJPGp8wfb1pugBQ#q=o+que+%C3%A9+uma+pokedex%3F)).

![Ilustração da Pokédex](http://vignette4.wikia.nocookie.net/pokepediabr/images/b/b7/Pok%C3%A9dex_GenI_Kanto.png/revision/latest?cb=20131222210519&path-prefix=pt-br)

# Funcionalidades

1. Listagem dos Pokemons: uma lista com uma imagem, numero e o nome.
2. Filtro de Pokémon por nome ou por número
2. Mostrar apenas os 151 originais
2. Tocando em um pokémon, o app mostra a tela de detalhes
3. Tela de detalhes:
	- Swipe com as imagens do pokemon
	- Titulo e Descrição do pokémon
	- Tipos
	- Atributos
	- Habilidades
	- Tudo o que você julgar de interessante (opcional)
4. Favoritar/Desfavoritar Pokémon
5. Mostrar os favoritos primeiro
6. O design é um diferencial

# De onde vem os Pokémons?

[The Poké API - The RESTful Pokémon API](https://pokeapi.co/)

# Procedimento (!!)

Faça um clone deste repositório ([Seleção Android](https://bitbucket.org/liveonsolutions/selecao-android/)), crie o seu repositório e faça push para o seu remote. Dê-nos acesso ao seu repositório do Bitbucket. Quando fizer isto, avise-me por e-mail [joel@superstarselfie.io](mailto:joel@superstarselfie.io?subject=Seleção%20Android), avaliarei o mais rápido possível e te daremos o feedback.


*Boa sorte!*
